package com.example.catalog

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs

class CatalogNextLevelFragment : Fragment(R.layout.fragment_catalog_next_level) {

    private val args: CatalogNextLevelFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.argument).text = "Argument ${args.id.toString()}"
    }

}