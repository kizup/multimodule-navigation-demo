package com.example.catalog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.catalog.router.CatalogRouter

class CatalogHostFragment : Fragment(R.layout.fragment_catalog_host) {

    private val navHostFragment: NavHostFragment
        get() = childFragmentManager.findFragmentById(R.id.catalog_host) as NavHostFragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeRouterUri()
    }

    private fun observeRouterUri() {
        CatalogRouter.uriLiveData.observe(viewLifecycleOwner) { uri ->
            navHostFragment.findNavController().navigate(uri)
        }
    }

}