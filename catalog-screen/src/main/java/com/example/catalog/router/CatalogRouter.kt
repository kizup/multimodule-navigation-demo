package com.example.catalog.router

import android.net.Uri
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.catalog.CatalogHostFragment
import com.example.core.navigation.IRouter
import com.example.core.navigation.IScreenEvent
import com.example.core.navigation.ScreenConstants

object CatalogRouter : IRouter {

    // Здесь перечисляем диплинки, которые может обработать данный роутер (либо заменить только на path)
    private val deepLinks = setOf(
        ScreenConstants.catalogUri,
        ScreenConstants.catalogNextLevelUri
    )

    private val mutableUriLiveData = MutableLiveData<Uri>()
    val uriLiveData: LiveData<Uri> get() = mutableUriLiveData

    override val screenEvent: IScreenEvent
        get() = CatalogScreenEvent

    override fun canHandleUri(uri: Uri): Boolean {
        return deepLinks.contains(uri)
    }

    override fun navigateTo(uri: Uri) {
        mutableUriLiveData.value = uri
    }

    private object CatalogScreenEvent : IScreenEvent {
        override val hostFragment: Fragment
            get() = CatalogHostFragment()
    }

}