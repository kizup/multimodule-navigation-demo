package com.example.catalog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.core.navigation.INavigator
import com.example.core.navigation.ScreenConstants

internal class CatalogFirstFragment : Fragment(R.layout.fragment_catalog_first) {

    private val iNavigator: INavigator by lazy {
        requireActivity() as INavigator
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<View>(R.id.to_next_level_button).setOnClickListener {
            findNavController().navigate(ScreenConstants.catalogNextLevelUri)
        }
        view.findViewById<View>(R.id.to_internal_screen_button).setOnClickListener {
            findNavController().navigate(R.id.action_catalogFirstFragment_to_internalNavigationFragment)
        }
    }

}