package com.example.multimodulesnavigationtest

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.catalog.router.CatalogRouter
import com.example.core.navigation.INavigator
import com.example.multimodulesnavigationtest.navigation.GlobalApplicationRouter
import com.example.rootscreen.RootFragment

class MainActivity : AppCompatActivity(R.layout.activity_main),
    INavigator by GlobalApplicationRouter {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            observeRouterLiveData()
            setupAppRouter()
        }
        GlobalApplicationRouter.processIncomingIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let { GlobalApplicationRouter.processIncomingIntent(it) }
    }

    private fun observeRouterLiveData() {
        GlobalApplicationRouter.screenEventLiveData.observe(this) { event ->
            val transaction = supportFragmentManager.beginTransaction()
            with(transaction) {
                replace(R.id.host_fragment, event.hostFragment)
                val stackCount = supportFragmentManager.fragments.size
                if (stackCount > 0) {
                    addToBackStack(event.hostFragment.javaClass.simpleName)
                }
                commitAllowingStateLoss()
            }
        }
    }

    // Эта инициализация по идее должна происходить 1 раз
    private fun setupAppRouter() {
        GlobalApplicationRouter.registerRouter(CatalogRouter)
    }

}
