package com.example.multimodulesnavigationtest.navigation

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.core.navigation.INavigator
import com.example.core.navigation.IRouter
import com.example.core.navigation.IScreenEvent

// Эта сущность необязательно должна быть как объект, сделано в тестовом проекте для простоты.
// В реальном проекте нужно использовать DI
object GlobalApplicationRouter : INavigator {

    private val routes = HashSet<IRouter>()
    private val mutableScreenLiveData = MutableLiveData<IScreenEvent>()
    val screenEventLiveData: LiveData<IScreenEvent> get() = mutableScreenLiveData

   fun registerRouter(router: IRouter) {
       routes.add(router)
   }

    override fun navigateTo(uri: Uri) {
        internalNavigateToUri(uri)
    }

    fun processIncomingIntent(intent: Intent) {
        val uri = intent.data
        if (uri == null) {
            navigateToRoot()
            return
        }
        internalNavigateToUri(uri)
    }

    private fun internalNavigateToUri(uri: Uri) {
        val routerThatCanHandleUri = routes.firstOrNull { it.canHandleUri(uri) }
        if (routerThatCanHandleUri == null) {
            // Вариантов обработки может быть несколько, один из них - открывать рутовый экран
            navigateToRoot()
        } else {
            // Эмитим новый хост, чтобы активити добавила отобразила его в контейнере
            emitNavigationScreen(routerThatCanHandleUri.screenEvent)
            // Просим текущий роутер обработать нашу ссылку
            routerThatCanHandleUri.navigateTo(uri)
        }
    }

    private fun emitNavigationScreen(screen: IScreenEvent) {
        mutableScreenLiveData.value = screen
    }

    private fun navigateToRoot() {
        emitNavigationScreen(RootScreenEvent())
    }

}