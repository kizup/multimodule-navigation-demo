package com.example.multimodulesnavigationtest.navigation

import androidx.fragment.app.Fragment
import com.example.core.navigation.IScreenEvent
import com.example.rootscreen.RootFragment

class RootScreenEvent : IScreenEvent {
    override val hostFragment: Fragment
        get() = RootFragment()
}