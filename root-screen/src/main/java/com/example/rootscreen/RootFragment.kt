package com.example.rootscreen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.core.navigation.INavigator
import com.example.core.navigation.ScreenConstants

class RootFragment : Fragment(R.layout.fragment_root_screen) {

    private val iNavigator: INavigator by lazy {
        requireActivity() as INavigator
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<View>(R.id.to_next_screen_button).setOnClickListener {
            iNavigator.navigateTo(ScreenConstants.catalogUri)
        }
        view.findViewById<View>(R.id.to_next_level_catalog_button).setOnClickListener {
            iNavigator.navigateTo(ScreenConstants.catalogNextLevelUri)
        }
    }

}