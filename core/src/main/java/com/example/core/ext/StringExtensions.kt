package com.example.core.ext

import android.net.Uri

internal fun String.toUri(): Uri = Uri.parse(this)