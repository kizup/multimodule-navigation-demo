package com.example.core.navigation

import android.net.Uri

interface IRouter {

    val screenEvent: IScreenEvent

    fun canHandleUri(uri: Uri): Boolean

    fun navigateTo(uri: Uri)

}