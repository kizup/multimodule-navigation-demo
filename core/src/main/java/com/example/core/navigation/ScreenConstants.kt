package com.example.core.navigation

import com.example.core.ext.toUri

object ScreenConstants {

    val catalogUri = "example://catalog/".toUri()
    val catalogNextLevelUri = "example://catalog/nextlevel".toUri()

}