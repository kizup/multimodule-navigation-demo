package com.example.core.navigation

import android.net.Uri

interface INavigator {

    fun navigateTo(uri: Uri)

}