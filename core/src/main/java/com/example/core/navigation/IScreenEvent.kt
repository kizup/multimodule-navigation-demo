package com.example.core.navigation

import androidx.fragment.app.Fragment

interface IScreenEvent {
    val hostFragment: Fragment
}